import axios from "axios";
import { useNavigate } from "react-router";
function ButtonPartnership(props){
    let navigate = useNavigate();

    function buttonHandler(e){
        e.preventDefault();
        if(props.data.store_name === "" || props.data.city === ""){
            alert("invalid data");
        }else{
            let user = JSON.parse(localStorage.getItem('user'));
            axios.post('/api/store', {
                store_name: props.data.store_name,
                city: props.data.city,
                user:{
                    user_id: user.user_id
                }
            }).then(res => {
                console.log(res);
                navigate({pathname: "/store"})
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
    return(
        <div>
            <button onClick={ buttonHandler } className="custom-button">Partnership</button>
        </div>
    )
}

export default ButtonPartnership;