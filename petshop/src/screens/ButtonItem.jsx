import axios from 'axios';

const ButtonItem = (props) => {
    console.log(props.data);
    console.log(props.imageStatus);

    if(props.imageStatus === true){
        console.log(props.image.name)
    }

    function buttonHandler(e){
        e.preventDefault();
        axios.post('/api/product', {
            product_name: props.data.product_name,
            price: props.data.price,
            description: props.data.description,
            image: props.data.image,
            store: {
                store_id: props.data.store.store_id
            }
        })

        const formData = new FormData();
        formData.append('image', props.image)
        axios.post('/api/product/upload_image',formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        });

        

        alert('Product added');
    }

    return(
        <div>
            <button className="custom-button" onClick={buttonHandler}>Submit</button>
        </div>

    );
}

export default ButtonItem;