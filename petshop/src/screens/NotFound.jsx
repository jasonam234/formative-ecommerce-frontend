import { Link, useNavigate } from "react-router-dom";
import NotFoundImage from '../assets/images/not_found.jpg'
import './NotFound.css'


const NotFound = () => {
    let navigate = useNavigate();

    return(
        <div>
            <img src={NotFoundImage} className="image-middle"/>
            <br/>
            <Link to="/">
                <p className="margin-middle">Click Here to Go To Login</p>
            </Link>
        </div>
        
    );
}

export default NotFound;