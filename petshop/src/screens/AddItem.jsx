import NavBarStore from "../components/NavBarStore";
import { Card } from "react-bootstrap";
import FormItem from "./FormItem";
import Footer from "../components/Footer";
import './AddItem.css'
import FooterStore from "../components/FooterStore";

const AddItem = () => {
    return(
        <div>
            <NavBarStore/>
                <Card className="custom-background center-screen-form-item" border="light"> 
                    <FormItem />
                </Card>
            <FooterStore/>
        </div>
    )
}

export default AddItem;