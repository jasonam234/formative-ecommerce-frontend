import NavBar from "../components/NavBar";
import Footer from "../components/Footer";
import axios from 'axios';
import { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { Link, useNavigate } from "react-router-dom";
import { Carousel } from "react-bootstrap";
import "./Dashboard.css";
import SlideOne from '../assets/images/slide_one.jpg'
import SlideTwo from '../assets/images/slide_two.jpg'
import SlideThree from '../assets/images/slide_three.jpg'


const Dashboard = () => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    let navigate = useNavigate();

    useEffect(() => {
        setIsLoading(true);
        axios.get("/api/store/true")
            .then(res => {
                const data = res.data.data;
                setTimeout(() => { setIsLoading(false); }, 2000);

                setData(data);
            })

    }, [setIsLoading, setData])

    console.log(isLoading);

    if (isLoading) {
        return (
            <div>
                <NavBar />
                <Carousel variant="dark">
                    <Carousel.Item>
                        <Skeleton width='1000px' height='200px' className="custom-slide-size"/>
                    </Carousel.Item>
                </Carousel>
                <br/>
                <Row className="mt-4" xs={1} md={2} className="g-4 horizontal-margin justify-content-md-center">
                    {Array.from({ length: 4 }).map((_, idx) => (
                        <Col style={{width:'18rem'}}>
                            <Card>
                                <Card.Body>
                                    <Card.Title><Skeleton /></Card.Title>
                                    <Card.Text><Skeleton /></Card.Text>
                                    <Card.Text><Skeleton /></Card.Text>
                                    <Card.Text><Skeleton /></Card.Text>
                                    <Card.Text><Skeleton /></Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Footer />
            </div>
        );
    }

    function buttonHandler(storeId) {
        navigate({ pathname: `/product/${storeId}` })
    }

    function slide() {
        return (
            <Carousel variant="dark">
                <Carousel.Item>
                    <img
                        // className="d-block w-100"
                        src={SlideOne}
                        alt="First slide"
                        className="custom-slide-size"
                        style={{height:"200px", width:"1000px"}}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        // className="d-block w-100"
                        src={SlideTwo}
                        alt="second slide"
                        className="custom-slide-size"
                        style={{height:"200px", width:"1000px"}}
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        // className="d-block w-100"
                        src={SlideThree}
                        alt="second slide"
                        className="custom-slide-size"
                        style={{height:"200px", width:"1000px"}}
                    />
                </Carousel.Item>
            </Carousel>
        )
    }

    return (
        <div>
            <NavBar />
            {slide()}
            <br/>
            <Row className="mt-4" xs={1} md={2} className="g-4 horizontal-margin justify-content-md-center">
                {data.map((value, index) => (
                    <Col key={index} style={{width:'18rem'}}>
                        <Card>
                            <Card.Body>
                                <Card.Title>{value.store_name}</Card.Title>
                                <Card.Text>{value.city}</Card.Text>
                                <Card.Text>{value.user.detailUser.fullname}</Card.Text>
                                <Card.Text>{value.user.detailUser.phone_number}</Card.Text>
                                <Button onClick={() => buttonHandler(value.store_id)} variant="primary">Store</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
            <br/>
            <br/>
            <br/>
            <Footer />
        </div>
    );
}

export default Dashboard;