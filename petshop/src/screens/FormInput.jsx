import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form'
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import './FormInput.css';
import ButtonCustom from './ButtonCustom';
import ButtonToRegister from './ButtonToRegister';

const FormInput = (props) => {
    let [data, setData] = useState(
        {
            username:"",
            password:""
        }
    );

    function usernameHandler(events){
        console.log(events.target.value)
        setData({
            ...data, username: events.target.value
        });
    }

    function passwordHandler(events){
        console.log(events.target.value)
        setData({
            ...data, password: events.target.value
        });
    }

    return(
        <Container>
            <Row>
                <Col>
                    <div>
                        <p className="color-text">Username</p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <Form.Group className="mb-3" onChange={ usernameHandler } controlId="exampleForm.ControlInput1">
                            <Form.Control type="text" placeholder="johndoe@email.com" />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div>
                        <p className="color-text">Password</p>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Form>
                        <Form.Group className="mb-3" onChange={ passwordHandler } controlId="exampleForm.ControlInput2">
                            <Form.Control type="password" placeholder="password" />
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
            <ButtonCustom data={data} function={props.function}/>
            <hr/>
            <ButtonToRegister/>
        </Container>
    );
}

export default FormInput;