import Login from './Login';
import Dashboard from './Dashboard';
import { useState, useEffect } from 'react';
import { Route, Router } from "react-router";
import FormRegister from './FormRegister';
import Register from './Register';
import DashboardAdmin from './DashboardAdmin';
import PartnerShip from './PartnerShip';

const Landing = () => {
    const [isLogin, setLogin] = useState(true);
    
    function setLoginStatus(payload){
        console.log(payload);
        setLogin(false);
    }

    return(
        <div>
            {isLogin === true ?  <Login function={setLoginStatus}/> : <Dashboard />}
        </div>
    );
}

export default Landing;