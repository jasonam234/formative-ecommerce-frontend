import { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import FormInput from './FormInput';
import './Login.css';
import logo from '../assets/images/logo.png'


const Login = (props) => {
    

    return(
        <Container>
            <Row>
                <Col className="custom-padding-card">
                    <Card className="custom-background center-screen-form" border="light">
                        <center>
                            <h5>Login</h5>
                            <hr/>
                        </center>
                    <img className="custom-image-size" src={logo}/>
                        <Card.Body>
                            <FormInput function={props.function}/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default Login;