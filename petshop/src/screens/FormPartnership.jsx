import { useState } from "react";
import { Row, Col, Container, Form } from "react-bootstrap";
import ButtonPartnership from "./ButtonPartnership";
import "./FormPartnership.css"

function FormPartnership(props) {
    const [store, setStore] = useState(
        {
            store_name: "",
            city: "",
            user: {
                user_id: ""
            }
        })

    function store_nameHandler(events) {
        console.log(events.target.value);
        setStore((prevsValue) => {
            return {
                ...prevsValue,
                store_name: events.target.value
            }
        })
    }

    function cityHandler(events) {
        console.log(events.target.value);
        setStore((prevsValue) => {
            return {
                ...prevsValue,
                city: events.target.value
            }
        })
    }

    return (
        <div className="center-form">
            <Container>
                <h2 className="center-text">Register Store</h2>
                <hr />
                <Row>
                    <Col>
                        <Form>
                            <p className="color-text">Store name</p>
                            <Form.Group className="mb-3" controlId="formBasicText">
                                <Form.Control type="text" placeholder="Store name" onChange={store_nameHandler} />
                            </Form.Group>
                            <p className="color-text">City</p>
                            <Form.Group className="mb-3" controlId="formBasicText">
                                <Form.Control type="text" placeholder="City" onChange={cityHandler} />
                            </Form.Group>
                            <ButtonPartnership data={store} function={props.function}/>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default FormPartnership;