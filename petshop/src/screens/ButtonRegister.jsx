import './ButtonCustom.css'
import axios from 'axios';
import { useNavigate } from 'react-router';

const ButtonRegister = (props) => {
    const validPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%\^&\*])(?=.{8,})');
    const validUsername = new RegExp('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$');
    const validNumberPhone = new RegExp('[0-9]{10,}');
    let navigate = useNavigate();

    function buttonHandler(e){
        e.preventDefault();
        if(props.data.username === "" || props.data.password === "" || props.data.detailUser.fullname === ""
        || props.data.detailUser.address === "" || props.data.detailUser.phone_number ===""){
            alert("invalid data");
        }else if(!validPassword.test(props.data.password)){
            alert("invalid password");
        }else if(!validUsername.test(props.data.username)){
            alert("invalid username")
        }else if(!validNumberPhone.test(props.data.detailUser.phone_number)){
            alert("invalid phone number")
        }else{
            axios.post('/api/user', {
                username: props.data.username,
                password: props.data.password,
                detailUser:{
                    fullname: props.data.detailUser.fullname,
                    address: props.data.detailUser.address,
                    phone_number: props.data.detailUser.phone_number
                }
            }).then(res => {
                console.log(res);
                let data = res.data.data[0];
                localStorage.setItem('user', JSON.stringify(data));
                navigate({pathname: "/dashboard"})
            }).catch(function (error) {
                console.log(error);
                alert("please choose another username")
            });

        }
    }

    return(
        <div>
            <button className="custom-button" onClick={buttonHandler}>Sign in</button>
        </div>
        
    );
}

export default ButtonRegister;