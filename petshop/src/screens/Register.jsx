import NavBar from "../components/NavBar";
import FormRegister from "./FormRegister";
import Footer from "../components/Footer";
import { useState } from "react";
import { Card } from "react-bootstrap";
import './Register.css';

function Register(props){

    return(
        <div>
            <Card className="custom-background-register center-screen-form-register" border="light"> 
                <FormRegister function={props.function} />
            </Card>
        </div>
    )
}

export default Register;