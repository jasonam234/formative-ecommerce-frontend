import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import ButtonItem from "./ButtonItem";
import { useState } from 'react';
import './FormItem.css'

const FormItem = () => {
    let [data, setData] = useState(
        {
            "product_name": "",
            "price": 0,
            "description": "",
            "image": "",
            "store": {
                "store_id": localStorage.getItem("store_id")
            }
        }
    );

    let [image, setImage] = useState();
    let [isUploaded, setIsUploaded] = useState(false);

    function productNameHandler(events){
        console.log(events.target.value);
        setData((prevsValue)=>{
            return {
                ...prevsValue,
                product_name :events.target.value
            }
        })
    }

    function priceHandler(events){
        console.log(events.target.value);
        setData((prevsValue)=>{
            return {
                ...prevsValue,
                price :events.target.value
            }
        })
    }

    function descriptionHandler(events){
        console.log(events.target.value);
        setData((prevsValue)=>{
            return {
                ...prevsValue,
                description :events.target.value
            }
        })
    }

    function imageHandler(events){
        const filename = events.target.value.replace(/^.*\\/, "");
        console.log(filename);
        setData((prevsValue)=>{
            return {
                ...prevsValue,
                image :filename
            }
        })
        setImage(events.target.files[0]);
        setIsUploaded(true);
    }

    return (        
        <div className="center-form">
            <Container>
            <h2 className="center-text">Add Product</h2>
            <hr />
                <Row>
                    <Col>
                        <Form className="input">
                            <p className="color-text">Product Name</p>
                            <Form.Group className="mb-3" controlId="formProductName">
                                <Form.Control type="text" placeholder="Product Name" onChange={productNameHandler}/>
                            </Form.Group>
                            <p className="color-text">Price</p>
                            <Form.Group className="mb-3" controlId="formBasicPrice">
                                <Form.Control type="number" placeholder="Price" onChange={priceHandler}/>
                            </Form.Group>
                            <p className="color-text">Description</p>
                            <Form.Group className="mb-3" controlId="formBasicDescription">
                                <Form.Control type="text" placeholder="Description" onChange={descriptionHandler}/>
                            </Form.Group>
                            <p className="color-text">Product Image</p>
                            <Form.Group controlId="formFile" className="mb-3" >
                                <Form.Control type="file" onChange={imageHandler}/>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Container>
            <ButtonItem data={data} imageStatus={isUploaded} image={image}/>
        </div>
    )
}

export default FormItem;