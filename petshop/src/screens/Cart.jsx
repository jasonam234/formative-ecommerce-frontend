import { Card, Row, Col, Button } from 'react-bootstrap';
import Footer from '../components/Footer';
import NavBar from '../components/NavBar';
import CurrencyFormat from 'react-currency-format';
import './Cart.css'
import { Link, useNavigate } from "react-router-dom";
import EmptyCartImage from '../assets/images/empty_cart.jpg'

const Cart = () => {
    let navigate = useNavigate();
    let carts = JSON.parse(localStorage.getItem('carts'));
    let totalPrice = 0;
    var CurrencyFormat = require('react-currency-format');

    const buttonHandler = () => {
        navigate(
            {
                pathname: "/checkout"
            },
            {state: {total: totalPrice}}
        )
    }

    console.log(localStorage.getItem('carts'));

    if(carts === null){
        return(
            <div>
                <NavBar/>
                    <img src={EmptyCartImage} className="image-middle"/>
                <Footer/>
            </div>
        );
    }else{
        totalPrice = carts.reduce((total,price)=> total + price.price, 0);
        return(
            <div>
                <NavBar/>
                <Row xs={1} md={5} className="g-4 margin-top horizontal-margin justify-content-md-center">
                    {carts.map((value,index) => {
                        const image_url = "http://127.0.0.1:8081/api/product/image/" + value.image;
                        return(
                            <Col key={index} className="margin-product">
                                <Card border="light" className="cart-product">
                                <Card.Body>
                                <Card.Img className = "custom-image-size-product " variant="top" src={image_url} />
                                    <Card.Title>{value.product_name}</Card.Title>
                                    <Card.Text>{value.store.store_name}</Card.Text>
                                    <Card.Text>{value.store.city}</Card.Text>
                                    <Card.Text>{value.description}</Card.Text>
                                    <Card.Text>{value.price}</Card.Text>    
                                </Card.Body>        
                                </Card>    
                            </Col>    
                        )    
                    })

                    }
                </Row>     
                <hr/>
                <Row className="g-4 margin-top horizontal-margin justify-content-md-center">
                    <Col className="center-text horizontal-margin">
                        <h5>Total Price :<CurrencyFormat value={totalPrice} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></h5>
                    </Col>
                </Row>
                <Row className="g-4 margin-top horizontal-margin justify-content-md-center">
                    <Col className="center-text horizontal-margin">
                        <Button onClick={buttonHandler}>Checkout</Button>
                    </Col>
                </Row>
                <hr/>
                <br/>
                <br/>
                <br/>
                <Footer/>
            </div>
        );
    }
}

export default Cart;