import Form from 'react-bootstrap/Form'
import { Container, Row, Col, Button } from 'react-bootstrap';
import ButtonRegister from './ButtonRegister';
import { useState } from 'react';
import './FormRegister.css';

function FormRegister(props) {
    const [user, setUser] = useState(
        {
            username: "",
            password: "",
            role: {
                role_id: 1,
                role_name: "User",
            },
            detailUser: {
                fullname: "",
                address: "",
                phone_number: "",
            }
        }
    );

    function usernameHandler(events) {
        console.log(events.target.value);
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                username: events.target.value
            }
        })
    }

    function passwordHandler(events) {
        console.log(events.target.value);
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                password: events.target.value
            }
        })
    }

    function fullNameHandler(events) {
        console.log(events.target.value);
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                detailUser: {
                    ...prevsValue.detailUser,
                    fullname: events.target.value
                }
            }
        })
    }

    function addressHandler(events) {
        console.log(events.target.value);
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                detailUser: {
                    ...prevsValue.detailUser,
                    address: events.target.value
                }
            }
        })
    }

    function phoneNumberHandler(events) {
        console.log(events.target.value);
        setUser((prevsValue) => {
            return {
                ...prevsValue,
                detailUser: {
                    ...prevsValue.detailUser,
                    phone_number: events.target.value
                }
            }
        })
    }

    return (
        <div className="center-form">
            <Container>
                <h2 className="center-text">Create a new account</h2>
                <hr />
                <Row>
                    <Col>
                        <Form className="input">
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <p className="color-text">Username</p>
                                <Form.Control type="email" placeholder="Username" onChange={usernameHandler} />
                            </Form.Group>
                        </Form>
                    </Col>
                    <Col>
                        <Form className="input">
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <p className="color-text">Password</p>
                                <Form.Control type="password" placeholder="Password" onChange={passwordHandler} />
                            </Form.Group>

                        </Form>
                    </Col>
                </Row>
                <Row>
                    <hr />
                </Row>
                <Row>
                    <Col>
                        <Form.Group className="mb-3" controlId="formBasicText">
                            <p className="color-text">Full Name</p>
                            <Form.Control type="text" placeholder="Full Name" onChange={fullNameHandler} />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group className="mb-3" controlId="formBasicText">
                            <p className="color-text">Address</p>
                            <Form.Control type="text" placeholder="Address" onChange={addressHandler} />
                        </Form.Group>
                    </Col>
                    <Col>
                        <Form.Group className="mb-3" controlId="formBasicText">
                            <p className="color-text">Phone Number</p>
                            <Form.Control type="number" placeholder="Phone Number" onChange={phoneNumberHandler} />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <hr/>
                    <ButtonRegister data={user} function={props.function} />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default FormRegister;