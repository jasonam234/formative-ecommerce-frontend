import { useState, useEffect } from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import { useParams } from "react-router-dom";
import logo from '../assets/images/placeholder_logo.png'
import "./Product.css"
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'

const Product = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const params = useParams();

    console.log(params);

    useEffect(() => {
        setIsLoading(true);
        axios.get(`/api/products/${params.productId}`)
        .then(res => {
            const data = res.data.data;
            setData(data);
            setTimeout(() => {  setIsLoading(false);  }, 2000);
        })
    }, [setData])

    function buttonHandler(value){
        // localStorage.setItem('cart', JSON.stringify(value));
        let carts = [];
        if(localStorage.getItem('carts')){
            carts = JSON.parse(localStorage.getItem('carts'));
        }
        carts.push(value);
        localStorage.setItem('carts', JSON.stringify(carts));
        alert("Item added to cart");
    }

    if(isLoading){
        return(
            <div>
                <NavBar/>
                <Row xs={1} md={5} className="g-4 margin-top horizontal-margin justify-content-md-center">
                    {Array.from({ length: 4 }).map((_, idx) => (
                        <Col className="margin-product">
                        <Card border="light" style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Text><Skeleton/></Card.Text>
                                <Card.Text><Skeleton/></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                            </Card.Body>
                        </Card>
                        </Col>
                    ))}
                </Row>
                <br/>
                <br/>
                <br/>
                <Footer/>
            </div>
        );
    }

    return(
        <div>
        <NavBar/>

        <Row xs={1} md={5} className="g-4 margin-top horizontal-margin justify-content-md-center">
        {data.map((value, index) => {
                const image_url = "http://127.0.0.1:8081/api/product/image/" + value.image;
                return(
                    <Col key={index} className="margin-product">
                        <Card border="light" className="cart-product">
                            <Card.Body>
                                <Card.Img className = "custom-image-size-product" variant="top" src={image_url} />
                                <Card.Title>Name: {value.product_name}</Card.Title>
                                <Card.Text>Description : {value.description}</Card.Text>
                                <Card.Text>Price : {value.price}</Card.Text>
                                <Button onClick={ () => buttonHandler(value) } variant="primary">Add to Cart</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                );
            })}
        </Row>
        <br/>
        <br/>
        <br/>

        <Footer/>
        </div>
    );
}

export default Product;