import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import axios from 'axios';
import { useLocation } from "react-router-dom";
import { useState, useEffect } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import './Checkout.css'
import CurrencyFormat from 'react-currency-format';
import { Link, useNavigate } from "react-router-dom";

const Checkout = () => {
    let location = useLocation();
    let navigate = useNavigate();
    let carts = JSON.parse(localStorage.getItem('carts'));
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);


    console.log(location.state.total);

    function buttonHandler(){
        const user = JSON.parse(localStorage.getItem("user"));
        axios.post('/api/transaction/checkout', {
            grand_total: location.state.total,
            status: false,
            user: {
                user_id: user.user_id,
            }
        });
        alert("Thank you for your purchase !");
        localStorage.removeItem("carts");
        navigate({pathname: "/dashboard"});
    }

    return(
        <div>
            <NavBar />
            <br/>
            <Row className="justify-content-md-center center-text horizontal-margin">
                <Card style={{ width: '30rem' }}>
                    <Card.Body>
                        <Card.Text>
                            <p className="invoice-header">Grand Total : <CurrencyFormat value={location.state.total} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} /></p>
                        </Card.Text>
                        <hr />
                    {carts.map((value, index) => {
                        return(
                            <div key={index}>
                                <Card.Text>Product Name : {value.product_name}</Card.Text>      
                                <Card.Text>Store Name : {value.store.store_name}</Card.Text>
                                <Card.Text>Store City : {value.store.city}</Card.Text>
                                <Card.Text>Product Price : {value.price}</Card.Text>     
                                <hr/>
                            </div>
                        );

                    })}
                    <Button onClick={buttonHandler}>Pay</Button>
                    </Card.Body>
                </Card>
                
            </Row>
            <br/>
            <br/>
            <br/>
            <Footer/>
        </div>

    );
}

export default Checkout;