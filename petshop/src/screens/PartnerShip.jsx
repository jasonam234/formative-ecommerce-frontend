import NavBar from "../components/NavBar";
import { Card } from "react-bootstrap";
import Footer from "../components/Footer";
import FormPartnership from "./FormPartnership";
import "./PartnerShip.css"

function PartnerShip(props){
        return(
            <div>
                <NavBar/>
                <Card className="custom-background-partnership center-screen-form-partnership" border="light"> 
                <FormPartnership function={props.function} />
                </Card>
                <Footer/>
            </div>
        )
}

export default PartnerShip;