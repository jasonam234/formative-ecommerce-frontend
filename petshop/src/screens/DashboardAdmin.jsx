import Footer from "../components/Footer";
import NavBar from "../components/NavBar";
import { useState, useEffect } from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import Button from 'react-bootstrap/Button';
import NavBarAdmin from "../components/NavBarAdmin";
import FooterAdmin from "../components/FooterAdmin";

const DashboardAdmin = () => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true)
        axios.get("/api/store/false")
        .then(res => {
            const data = res.data.data;
            setData(data);
            setIsLoading(false);
            // console.log(data);
        })
    })

    function buttonHandler(id){
        axios.put(`api/store/${id}`)
            .then(res => {
            console.log(res);
            const userId = res.data.data.user.user_id;
            console.log(userId);

            axios.put(`api/user/${userId}`)
            .then(res => {
                console.log(res);
            }).catch(function (error) {
                console.log(error);
            });
        }).catch(function (error) {
            console.log(error);
        });

        // axios.put(`api/user/${id}`)
        // .then(res => {
        // console.log(res);
        // }).catch(function (error) {
        //     console.log(error);
        // });
    }

    return(
        <div>
            <NavBarAdmin/>
            <br/>
            <Row className="mt-4" xs={1} md={2} className="g-4 horizontal-margin justify-content-md-center">
                {data.map((value, index) => (
                    <Col key={index} style={{width:'18rem'}}>
                        <Card>
                            <Card.Body>
                                <Card.Title>{value.store_name}</Card.Title>
                                <Card.Text>{value.city}</Card.Text>
                                <Card.Text>{value.user.detailUser.fullname}</Card.Text>
                                <Card.Text>{value.user.detailUser.phone_number}</Card.Text>
                                <Button onClick={() => buttonHandler(value.store_id)} variant="primary">Accept</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                ))}
            </Row>
            <FooterAdmin/>
        </div>
    );
}

export default DashboardAdmin;