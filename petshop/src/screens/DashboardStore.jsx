import { useState, useEffect } from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import NavBarStore from '../components/NavBarStore';
import "./Product.css"
import FooterStore from '../components/FooterStore';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import "./DashboardStore.css";

const DashboardStore = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    useEffect(() => {
        setIsLoading(true);
        const user = JSON.parse(localStorage.getItem("user"));
        console.log(user.user_id);
        axios.get(`/api/store/${user.user_id}`)
            .then(res => {
                const data = res.data.data.store_id;
                console.log(res.data.data.store_id);
                localStorage.setItem("store_id", data);

                axios.get(`/api/products/${data}`)
                .then(res => {
                    const data = res.data.data;
                    setData(data);
                    console.log(data);
                    setTimeout(() => {  setIsLoading(false);  }, 2000);
                })
            })
    }, [setData])

    if(isLoading){
        return(
            <div>
                <NavBarStore/>
                <Row xs={1} md={5} className="g-4 margin-top horizontal-margin justify-content-md-center">
                    {Array.from({ length: 4 }).map((_, idx) => (
                        <Col className="margin-product">
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Text><Skeleton/></Card.Text>
                                <Card.Text><Skeleton/></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                                <Card.Text><Skeleton /></Card.Text>
                            </Card.Body>
                        </Card>
                        </Col>
                    ))}
                </Row>
                <FooterStore/>
            </div>
        );
    }

    return(
        <div>
        <NavBarStore/>
        <Row xs={1} md={5} className="g-4 margin-top horizontal-margin justify-content-md-center">
        {data.map((value, index) => {
                const image_url = "http://127.0.0.1:8081/api/product/image/" + value.image;
                return(
                    <Col key={index} className="margin-product">
                        <Card border="light" className="cart-product">
                            <Card.Body>
                                <Card.Img className = "custom-image-size-product" variant="top" src={image_url}/>
                                <hr/>
                                <Card.Title>{value.product_name}</Card.Title>
                                <Card.Text>{value.description}</Card.Text>
                                <Card.Text style={{color:"purple"}}>{value.price}</Card.Text>
                                <Button variant="primary">Make Unavailable</Button>
                            </Card.Body>
                        </Card>
                    </Col>
                );
            })}
        </Row> 
        <br/>
        <br/>
        <br/>

        <FooterStore/>
        </div>
    );
}

export default DashboardStore;