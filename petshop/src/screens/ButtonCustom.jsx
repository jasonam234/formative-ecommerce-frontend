import './ButtonCustom.css'
import axios from 'axios';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from './Dashboard';
import { Link, useNavigate } from "react-router-dom";

const ButtonCustom = (props) => {
    const validPassword = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%\^&\*])(?=.{8,})');
    const validUsername = new RegExp('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$');
    let navigate = useNavigate();

    function buttonHandler(){
        if(props.data.username === "" || props.data.password === ""){
            alert("invalid data");
        }else if(!validPassword.test(props.data.password)){
            alert("invalid password");
        }else if(!validUsername.test(props.data.username)){
            alert("invalid username")
        }else{
            axios.post('api/auth/login', {
                username: props.data.username,
                password: props.data.password
            }).then(res => {
                let data = res.data.data[0];
                localStorage.setItem('user', JSON.stringify(data));
                props.function(false);
                console.log(data.role.role_id);

                if(data.role.role_id === 1){
                    navigate({pathname: "/dashboard"})
                }else if(data.role.role_id === 2){
                    navigate({pathname: "/store"})
                }else if(data.role.role_id === 3){
                    navigate({pathname: "/admin"})
                }
            }).catch(function (error) {
                alert("Wrong username or password");
                console.log(error);
            });
        }
    }

    return(
        <div>
            <button onClick={ buttonHandler } className="custom-button">Log In</button>
        </div>
    );
}

export default ButtonCustom;