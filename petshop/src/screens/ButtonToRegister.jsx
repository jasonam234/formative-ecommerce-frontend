
import { useNavigate } from 'react-router';

function ButtonToRegister(){
    let navigate = useNavigate();

    function buttonHandler(e){
        e.preventDefault();
        navigate({pathname: "/register"})
    }
    return(
        <button className="custom-button" onClick={buttonHandler}>Create New Account</button>
    )
}

export default ButtonToRegister;