import 'bootstrap/dist/css/bootstrap.min.css'
import { Route, Routes } from "react-router-dom";
import './App.css';
import Dashboard from './screens/Dashboard';
import DashboardAdmin from './screens/DashboardAdmin';
import DashboardStore from './screens/DashboardStore';
import AddItem from './screens/AddItem';
import Landing from './screens/Landing';
import Login from './screens/Login';
import Product from './screens/Product';
import Register from './screens/Register';
import PartnerShip from './screens/PartnerShip';
import Cart from './screens/Cart';
import Checkout from './screens/Checkout';
import NotFound from './screens/NotFound'

function App() {

  return (
    <Routes>
      <Route path="/" element={<Landing/>}/>
      <Route path="/dashboard" element={<Dashboard/>}/>
      <Route path="/admin" element={<DashboardAdmin/>}/>
      <Route path="/register" element={<Register/>}/>
      <Route path="/store" element={<DashboardStore/>}/>
      <Route path="/partnership" element={<PartnerShip/>}/>
      <Route path="/product/:productId" element={<Product/>}/>
      <Route path="/store/add_item" element={<AddItem/>}/>
      <Route path="/cart" element={<Cart/>}/>
      <Route path="/checkout" element={<Checkout/>}/>
      <Route path="*" element={<NotFound/>}/>
    </Routes>
  );
}

export default App;
