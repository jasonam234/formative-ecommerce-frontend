import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import './Footer.css'

const FooterAdmin = () => {
    return(
        <div className="custom-footer-background sticky-footer margin-top-footer"> 
            <Navbar color="dark">
                <Container>
                    <Navbar.Text className="justify-content-end">
                        Admin
                    </Navbar.Text>
                </Container>
            </Navbar>
        </div>
    );
}

export default FooterAdmin;