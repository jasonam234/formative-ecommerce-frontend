import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import './Footer.css'
import { Link } from 'react-router-dom';

const Footer = () => {
    

    return(
        <div className="custom-footer-background sticky-footer margin-top-footer">  
            <Navbar sticky="bottom" color="dark">
                <Container>
                    <Navbar.Text className="justify-content-end">
                        <Link to="/partnership" className="text-partnership mr-auto">Become Partner</Link>
                    </Navbar.Text>
                </Container>
            </Navbar>
        </div>
    );
}

export default Footer;