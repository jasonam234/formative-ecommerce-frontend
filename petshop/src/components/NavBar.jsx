import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import './NavBar.css'
import Nav from 'react-bootstrap/Nav'
import { Link, useNavigate } from "react-router-dom";
import logo from '../assets/images/logo.png';
import logoutImage from '../assets/images/logout.png';
import cart from '../assets/images/cart.png';

const NavBar = () => {
    let navigate = useNavigate();

    function logout(){
        localStorage.removeItem("user");
    }

    return(
        <Navbar className="custom-navbar-background">
            <Container>
                <Navbar.Brand className="ml-auto">
                    <Link to="/dashboard">
                        <img src={logo} width="50" height="50"></img>
                    </Link>
                </Navbar.Brand>
                <Nav className="ml-auto">
                    <Link to="/cart">
                        <img src={cart} width="50" height="50"></img>
                    </Link>
                    <Link to="/">
                        <img src={logoutImage} width="50" height="50" onClick={logout}></img>
                    </Link>
                </Nav>
            </Container>
        </Navbar>
    );
}

export default NavBar;